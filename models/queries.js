const mongoose=require('mongoose');
//Creating contactSchema
const querieSchema=new mongoose.Schema({
    name:{
        type:String,
        require:[true,"name is required"]

    },
    email:{
        type:String,
        required:true,
        unique:true
    },
    number:{
        type:Number,
        required:[true,"mobile number is required"]
    },
    address:{
        type:String
    },
    message:{
        type:String
    }
})
//Fitting queriesModel
const queriesModel=mongoose.model('queries',querieSchema);
module.exports=queriesModel;