const express=require('express');
const app=express();
const HoganExpress = require("hogan-express");

const mongoose=require('mongoose')
mongoose.connect('mongodb://localhost:27017/callforjobs')
.then(()=>console.log(`Mongo DB connected`))
.catch(()=>console.log(`Mongo DB not connected`));

//Express middleware
app.use(express.json());
app.use(express.static('dist/admin_dist'));

app.engine('html', HoganExpress);
// By default, Express will use a generic HTML wrapper (a layout) to render all your pages. If you don't need that, turn it off.
app.set('view options', {
    layout: true
});
app.set('layout', 'container');
app.set('views', __dirname + '/dist/admin_dist');
app.set('view engine', 'html');

const userRouter= require("./routes/user");
const queryRouter= require('./routes/querie');

//Router middleware
app.use('/users',userRouter);
app.use('/queries',queryRouter);

const port=process.env.PORT||3005;
app.listen(port,()=>console.log(`Server started on ${port}`))